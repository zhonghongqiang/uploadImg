

public class Test {

    public static void main(String args[]) {

        Object obj[] = { 'A', 'B', 'C', 'D'};

        //Graph graph = new MatrixGraph(obj);  
        Graph graph = new MatrixGraph(obj);
        graph.setDirected(true);

        //graph.addVex('F');  
        graph.addEdge('A','A',1000);
        graph.addEdge('A','B',5);
        graph.addEdge('A','C',1000);
        graph.addEdge('A','D',1000);

        graph.addEdge('B','A',50);
        graph.addEdge('B','B',1000);
        graph.addEdge('B','C',15);
        graph.addEdge('B','D',5);

        graph.addEdge('C', 'A', 30);
        graph.addEdge('C', 'B', 1000);
        graph.addEdge('C', 'C', 1000);
        graph.addEdge('C', 'D', 15);

        graph.addEdge('D', 'A', 15);
        graph.addEdge('D', 'B', 1000);
        graph.addEdge('D', 'C', 5);
        graph.addEdge('D', 'D', 1000);

        System.out.println(graph.printGraph());  
        //graph.removeEdge('A', 'C');
        //graph.removeVex('A');
        System.out.println(graph.printGraph()); 
        /*graph.removeVex('E');
        System.out.println(graph.printGraph());*/
        System.out.println(graph.dfs('B'));
        //System.out.println(graph.bfs('B'));
    }
}  