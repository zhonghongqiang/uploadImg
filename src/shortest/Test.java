package shortest;

/**
 * Created by zhonghongqiang on 2017-02-21.
 */
public class Test {
    public static void main(String[] args) throws Exception {
        GraphByMatrix graph = new GraphByMatrix(GraphByMatrix.UNDIRECTED_GRAPH, GraphByMatrix.ADJACENCY_MATRIX, 7);

        graph.addVertex("1");
        graph.addVertex("2");
        graph.addVertex("3");
        graph.addVertex("4");
        graph.addVertex("5");
        graph.addVertex("6");
        graph.addVertex("7");
//        graph.addVertex("8");


//        graph.addEdge("0", "1",1);
//        graph.addEdge("0", "3",1);

        graph.addEdge("1", "2",1);
        graph.addEdge("1", "4",1);

        graph.addEdge("2", "5",1);

        graph.addEdge("3", "4",1);
        graph.addEdge("3", "6",1);

        graph.addEdge("4", "5",1);
        graph.addEdge("4", "7",1);

//        graph.addEdge("5", "8",1);

        graph.addEdge("6", "7",1);

//        graph.addEdge("7", "8",1);

        int turnWeight = 3;

//        graph.addTurnMatrix("3","0","1",turnWeight);

        graph.addTurnMatrix("2","1","4",turnWeight);
//        graph.addTurnMatrix("0","1","4",turnWeight);

        graph.addTurnMatrix("1","2","5",turnWeight);

//        graph.addTurnMatrix("0","3","4", turnWeight);
        graph.addTurnMatrix("6","3","4", turnWeight);

        graph.addTurnMatrix("1","4","5",turnWeight);
        graph.addTurnMatrix("1","4","3",turnWeight);
        graph.addTurnMatrix("3","4","7",turnWeight);
        graph.addTurnMatrix("5","4","7",turnWeight);

//        graph.addTurnMatrix("4","5","8",turnWeight);
        graph.addTurnMatrix("4","5","2",turnWeight);

        graph.addTurnMatrix("3","6","7",turnWeight);

        graph.addTurnMatrix("4","7","6",turnWeight);
//        graph.addTurnMatrix("4","7","8",turnWeight);

//        graph.addTurnMatrix("5","8","7",turnWeight);

        System.out.println(graph.printGraph());
        graph.Dijkstra("2","6");
        //System.out.println();
        //graph.Dijkstra(3);
        //System.out.println();
        //graph.Dijkstra2(0);
        //System.out.println();
    }
}
